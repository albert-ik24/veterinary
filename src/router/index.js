import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/IndexPage.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/LoginPage')
  },
  {
    path: '/services',
    name: 'Services',
    component: () => import('../views/ServicesPage')
  },
  {
    path: '/user',
    name: 'User',
    component: () => import('../views/User/UserIndex'),
    children: [
      {
        path: 'info',
        name: 'UserInfo',
        component: () => import('../views/User/UserInfo'),
      },
      {
        path: 'edit',
        name: 'UserEdit',
        component: () => import('../views/User/UserEdit'),
      },
      {
        path: 'pets',
        name: 'UserPets',
        component: () => import('../views/User/UserPets'),
      },
      {
        path: 'pet/:id',
        name: 'SeparatePet',
        component: () => import('../views/User/SeparatePet'),
      },
      {
        path: 'assigned-reception',
        name: 'UserAssignedReception',
        component: () => import('../views/User/UserAssignedReception'),
      },
      {
        path: 'reception-history',
        name: 'UserReceptionHistory',
        component: () => import('../views/User/UserReceptionHistory'),
      },
      {
        path: 'appointment',
        name: 'UserAnAppointment',
        component: () => import('../views/User/UserAnAppointment'),
      },
    ],
  },
  {
    path: '/doctor',
    name: 'Doctor',
    component: () => import('../views/Doctor/DoctorIndex'),
  },
  {
    path: '/doctor/reception',
    name: 'DoctorReception',
    component: () => import('../views/Doctor/DoctorReception'),
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
